import {Selector, t} from "testcafe";

export class Index {
    constructor() {
        this.todoTable = Selector('#todo_table');
        this.newTodoInput = Selector('form input[name="name"]');
        this.newTodoButton = Selector('form input[type="submit"]');

        this.addTodo = async function (name) {
            await t.typeText(this.newTodoInput, name).click(this.newTodoButton);
        };

        this.deleteTodo = async function (name) {
            var deleteLink = this.todoTable.find('tr').withText(name).find('a');
            await t.click(deleteLink);
        };

        this.containsTodo = async function (name) {
            await t.expect(this.todoTable.find('td').withText(name).visible).ok();
        };

        this.containsTodoNot = async function (name) {
            await t.expect(this.todoTable.find('td').withText(name).exists).notOk()
        }
    }
}