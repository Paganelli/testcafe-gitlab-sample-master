# Introduction

this is a companion repository for the article found at [dedicatedcode.com](https://www.dedicatedcode.com/posts/gitlab-testcafe/)
 
# Usage
* fork the repository
* push some changes and watch the pipeline executing integration tests with testcafe

# Questions?
please simply contact me over at my site and i will try to answer all your questions